import pygame, sys
from pygame.locals import *
from transitions import Machine

pygame.init()
pygame.display.set_caption('Crossing Road')

W = 300
H = 300
ws = pygame.display.set_mode((W, H))
ds = ws.convert_alpha() 

FPS = 30 
fpsClock = pygame.time.Clock()
frameNum = 1;

whitecColor = (255, 255, 255)
greenColor = (0,255,0)
redColor = (255, 0, 0)
blueColor = (0, 0, 255)

        
#direction = 'right'

class Car:

    states = ['start', 'going_front', 'going_back', 'you_won' ]

    def __init__(self):
        self.direction= "right"
        self.trafficX = 10
        self.trafficY = H/2
        self.carX = W/2-25
        self.carY = H - 25
        
        self.crashY = (self.trafficY + 10) or ( self.trafficY - 10)

        

        self.machine = Machine(model=self, states=Car.states, initial='start')
        self.machine.add_transition(trigger='go_front',source='*',dest='going_front')
        self.machine.add_transition(trigger='go_back',source='*',dest='going_back')
        self.machine.add_transition(trigger='start_over',source='*',dest='start')

    def draw(self):
        pygame.draw.rect(ds, redColor, (self.trafficX, self.trafficY, 50, 25))
        pygame.draw.rect(ds, blueColor, (self.carX, self.carY, 50, 25))

    def setTraffic(self):
        if self.direction == 'right':
            self.trafficX += 5
            if self.trafficX == 270:
                self.direction = 'left'
        if self.direction == 'left':
            self.trafficX -= 5
            if self.trafficX == 10:
                self.direction = 'right'
        


    def update(self):
        if(self.state == 'going_front'):
            self.carY -= 5
        elif(self.state == 'going_back'):
            self.carY += 5
        elif(self.state == 'start'):
            self.carY = H - 25
        if(self.carY == 0):
            self.state = 'you_won'
        if(self.carY >= H - 25):
            self.state = 'start'

        if(self.carY == self.trafficY):
            if((self.carX >= self.trafficX - 50) and (self.carX <= self.trafficX + 50)):
                self.state = 'start'
                print ("You Died")





myCar = Car()
    
while True: 

    ws.fill((0, 0, 0))
    ds.fill((0, 0, 0))

    myCar.draw()
    myCar.setTraffic()
    myCar.update()


    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                myCar.go_front()
            if event.key == pygame.K_s:
                myCar.go_back()
            if event.key == pygame.K_a:
                myCar.start_over()
        print(myCar.state)



    ws.blit(ds, (0 ,0))
    
    pygame.display.update()
    fpsClock.tick(FPS)
    
