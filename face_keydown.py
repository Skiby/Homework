import pygame, sys
from pygame.locals import *

pygame.init()
pygame.display.set_caption('EmoFace')

W = 400
H = 300

ws = pygame.display.set_mode((W, H))

ds = ws.convert_alpha()

FPS = 30 # frames per second setting
fpsClock = pygame.time.Clock()
frameNum = 0


blackColor = (0, 0, 0)
redColor = (255, 0, 0)
blueColor = (0, 0, 255)
greenColor = (0, 255, 0)
yellowColor = (255, 255, 0)


class face:
    def __init__(self):
        self.setState('smile')
        self.faceXY = (int (W/2), (int (H/2)))
        self.rEye = (250, 100)
        self.lEye = (150, 100)
        self.keyEyes = False
                       
    def drawFace(self, color, xyPos, radius):
        pygame.draw.circle(ds, color, xyPos, radius)
        

    def setState(self, stateStr):
        self.state = stateStr

    def drawEyes(self, eyesBool):
        self.keyEyes = eyesBool

    def draw(self):
        if self.state == 'smile':
            self.drawFace((255, 255, 0, 255), self.faceXY, 150)
            self.drawFace((255, 0, 0, 255), self.rEye, 20)
            self.drawFace((255, 0, 0, 255), self.lEye, 20)
            pygame.draw.rect(ds, blueColor, (150, 180, 100, 50))
            
        elif self.state == 'angry':       
            self.drawFace((255, 255, 0, 255), self.faceXY, 150)
            self.drawFace((255, 0, 0, 255), self.rEye, 20)
            self.drawFace((255, 0, 0, 255), self.lEye, 20)
            pygame.draw.line(ds, greenColor, (150,220),(250,220), 10)

        elif self.state == 'crazy':
            self.drawFace((0, 255, 0, 255), self.rEye, 20)
            self.drawFace((0, 255, 0, 255), self.lEye, 20)
        

    def update(self):

        if self.keyEyes:
            self.setState('crazy')
            

        elif frameNum < 20:
            self.setState('smile')
        elif frameNum < 40:
            self.setState('angry')

            
                       
        
        
expression = face()

while True:
    ds.fill(blackColor)

    expression.update()
    expression.draw()


    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                expression.drawEyes(True)

        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_SPACE:
                expression.drawEyes(False)
        
    if frameNum < 40:
        frameNum += 1
    else:
        frameNum = 0

    ws.blit(ds, (0,0))
    pygame.display.update()
    fpsClock.tick(FPS)
